package com.example.ats.podengine;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Ats-Faraz on 05/12/2017.
 */

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private Button fbLogin, googleLogin, emailLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        fbLogin = findViewById(R.id.button3);
        googleLogin = findViewById(R.id.button4);
        emailLogin = findViewById(R.id.button5);

        fbLogin.setOnClickListener(this);
        googleLogin.setOnClickListener(this);
        emailLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.button3 :
                Toast.makeText(this,"Logged in through Facebook" , Toast.LENGTH_LONG).show();
                return;

            case R.id.button4 :
                Toast.makeText(this,"Logged in through Google" , Toast.LENGTH_LONG).show();
                return;

            case R.id.button5:
                login();
        }
    }

    private void login() {

    }
}
