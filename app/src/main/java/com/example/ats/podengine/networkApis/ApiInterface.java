package com.example.ats.podengine.networkApis;

import com.example.ats.podengine.model.PodcastEpisodes;
import com.example.ats.podengine.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Ats-Faraz on 07/12/2017.
 */

public interface ApiInterface {

    @POST("podengine-portlet.playlist/player/{userId}")
    Call<PodcastEpisodes> getPodcastEpisodes(@Path("userId") long userId);

    @POST("podengine-portlet.assetcategorypriority/login-authentication/{email}/{password}")
    Call<User> getLoginDetails(@Path("email") String email, @Path("password") String password);

}
