package com.example.ats.podengine;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.ats.podengine.model.PodcastEpisodes;
import com.example.ats.podengine.networkApis.ApiClient;
import com.example.ats.podengine.networkApis.ApiInterface;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ats-Faraz on 05/12/2017.
 */

public class FindContentActivity extends AppCompatActivity {
    private static final String TAG = FindContentActivity.class.getSimpleName();
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer simpleExoPlayer;
    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_content);

        simpleExoPlayerView =  findViewById(R.id.video_view);


        ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
        Call<PodcastEpisodes> call = apiInterface.getPodcastEpisodes(20454);

        call.enqueue(new Callback<PodcastEpisodes>() {
            @Override
            public void onResponse(Call<PodcastEpisodes> call, Response<PodcastEpisodes> response) {
                Log.d(TAG, " "+response);
               //url = response.body().getPodcastUrl();
            }

            @Override
            public void onFailure(Call<PodcastEpisodes> call, Throwable t) {

            }
        });

        //initializePlayer();
    }

    private void initializePlayer() {
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this),new DefaultTrackSelector(),new DefaultLoadControl());

        simpleExoPlayerView.setPlayer(simpleExoPlayer);

        Uri uri = Uri.parse(getString(R.string.video_url));
        MediaSource mediaSource = buildMediaSource(uri);
        simpleExoPlayer.prepare(mediaSource,true,false);
    }

    private MediaSource buildMediaSource(Uri uri){
        return new ExtractorMediaSource(uri,new DefaultHttpDataSourceFactory("ua"),new DefaultExtractorsFactory(),null,null);
    }
}
