package com.example.ats.podengine.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ats-Faraz on 07/12/2017.
 */

public class PodcastEpisodes {

    @SerializedName("podcastUrl")
    private String podcastUrl;

    @SerializedName("podcastDuration")
    private Integer podcastDuration;

    @SerializedName("podcastTitle")
    private String podcastTitle;

    @SerializedName("podcastImageLink")
    private String podcastImageLink;

    public PodcastEpisodes(String podcastUrl , int podcastDuration, String podcastTitle, String podcastImageLink) {
        this.podcastUrl = podcastUrl;
        this.podcastDuration = podcastDuration;
        this.podcastTitle = podcastTitle;
        this.podcastImageLink = podcastImageLink;
    }

    public String getPodcastUrl() {
        return podcastUrl;
    }

    public void setPodcastUrl(String podcastUrl) {
        this.podcastUrl = podcastUrl;
    }

    public Integer getPodcastDuration() {
        return podcastDuration;
    }

    public void setPodcastDuration(Integer podcastDuration) {
        this.podcastDuration = podcastDuration;
    }

    public String getPodcastTitle() {
        return podcastTitle;
    }

    public void setPodcastTitle(String podcastTitle) {
        this.podcastTitle = podcastTitle;
    }

    public String getPodcastImageLink() {
        return podcastImageLink;
    }

    public void setPodcastImageLink(String podcastImageLink) {
        this.podcastImageLink = podcastImageLink;
    }
}
