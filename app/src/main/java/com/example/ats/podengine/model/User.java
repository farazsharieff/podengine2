package com.example.ats.podengine.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ats-Faraz on 12/12/2017.
 */

public class User {

    @SerializedName("userId")
    private String userId;

    public User(String userId) {
        this.userId = userId;
    }

}
